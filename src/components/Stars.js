import React from 'react';
import Star from "./Star";
import '../App.css';

const Stars = ({count}) => {

    const createArray = count => {
        if (count < 1 || count > 5 || typeof count !== 'number') return null;

        const array = [];
        for (let i = 1; i <= count; i++) {
            array.push(i)
        }
        return array;
    };

    const array = createArray(count);

    if (!array) return null;

    return (
        <ul className="card-body-stars u-clearfix stars">
            {array.map(() => <li><Star/></li>)}
        </ul>
    );
};

export default Stars;