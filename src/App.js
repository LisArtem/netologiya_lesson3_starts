import React from 'react';
import Stars from "./components/Stars";
import './App.css';

const App = () => <Stars count={3}/>;

export default App;